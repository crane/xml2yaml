Readme

This project is created by Crane Apr. 2012

Purpose: C# component, use SAX to convert Xml document to Yaml format


The requirement of the project please refer:

	http://yaml.org/xml.html

For those who love YAML, but require buzz-word compliance or require XML angle brackets, there is a clean option -- a subset of XML which has YAML's information model, but XML's syntax. Since a YAML stream may not start with a '<' character, a YAML implementation could implement an implicit, automatic conversion of XML in this schema to the equivalent YAML. As a proof of concept, a XSLT Stylesheet is provided, along with the canonical invoice example in XML using this schema. This can also be thought of as a 'native language' binding, and boils down to a simple set of rules:

    Mappings are expressed as elements, where the mapping key is represented using the element's name and the mapping value as the element's content. This restriction implies that a given tag name may not occur twice within a given context and that the order of the tag names is not significant.

    Lists are expressed as an orderd sequence of elements having a special name, the underscore. In this case, and only in this case is the ordering of elements significant and duplicate element names (just the underscore) are permitted. With these two rules, the bulk of YAML can be serialized.

    Scalars are modeled as a single text node child of a given element. Thus, mixed-content is not allowable.

    Unlike YAML, XML requires a root element with a name. Since this isn't part of the YAML information model, the element name can be arbitrarly chosen and should be discarded by the YAX->YAML conversion utility, the default element name can be 'yaml' to make things clear when going from YAML to XML.

    In this binding, an anchor on a node is modeled via the "anchor" attribute. Then, subsequent occurances of this node can be referenced by an empty element with an "alias" attribute having value matching a previous anchor.

    Since YAML tags are URIs, they can be used as XML namespaces, although since only a limited 'taguri' is allowed in YAML, using a XML namespace as a YAML tag is not generally possible.

    Nested keys, typed keys, or keys not matching the XML name production are emulated through a pair of elements, one for the key, the other for the value following each other sequentially with names "_key" and "_value". All XML names beginning with underscore are reserved so that this hack is clearly a hack.

    YAML comments are modeled directly as XML comments, only that they cannot be used to break up a text node or occur in any order differently than an associated YAML file.

    Escaping via XML character entities is allowed, but all other forms of entities are not.

    XML attributes lacking a namespace considered as elements, thus <a b="c"/> is considered as <a><b>c</b></a>

    All other features of XML are forbidden. Intermediate whitespace between elements used for readability is ignored.


Support user-define configuration: 
	1. Namespace mapping
	2. Ignore xpath list in source file
	3. mapping xpath to target value

How to build:

Mono:
	mdtool build Xml2Yaml.sln

VS:
	msbuild Xml2Yaml.sln

