﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Configuration;

namespace UnitTest
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> runnerArgs = new List<string>();
            runnerArgs.Add(Assembly.GetExecutingAssembly().Location);
            runnerArgs.Add("/framework=net-4.0");

#if (DEBUG)
            string scope = ConfigurationManager.AppSettings["TestScope"];
            if (!string.IsNullOrEmpty(scope))
            {
                runnerArgs.Add(scope);
            }
#endif
            NUnit.ConsoleRunner.Runner.Main(runnerArgs.ToArray());
        }
    }
}
