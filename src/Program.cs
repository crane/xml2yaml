﻿using System;
using System.Diagnostics;
using System.IO;

namespace Xml2Yaml
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                //read from stand input and write to stand output
                using (YamlWriter sw = new YamlWriter(Console.OpenStandardOutput()))
                {
                    sw.Convert(Console.OpenStandardInput());
                }
            }
            else
            {
                string configFileName = args.Length > 1 ? args[1] : string.Empty;
                string outputFileName = args.Length > 2 ? args[2] : string.Empty;

                bool converted = false;
                using (Stream output = !string.IsNullOrEmpty(outputFileName)
                    ? YamlUtil.OpenOrCreateOutput(outputFileName)
                    : Console.OpenStandardOutput())
                {
                    //read from file name and write to stand output
                    using (YamlWriter sw = new YamlWriter(output, configFileName))
                    {
                        if (sw.Initialized)
                        {
                            converted = sw.Convert(args[0]);
                        }
                    }
                }

                if (converted && !string.IsNullOrEmpty(outputFileName))
                {
                    try
                    {
                        if (YamlUtil.IsWindowsPlatForm())
                        {
                            Process.Start("notepad.exe", outputFileName);
                        }
                        else
                        {
							//todo Handle on linux
                            Process.Start("more", outputFileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Console.Error.WriteLine(ex.Message);
                    }
                }
            }
        }
    }
}
