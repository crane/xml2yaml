﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xml2Yaml
{
    class YamlNode
    {
        public string Namespace;
        public string LocalName;
        public IDictionary<AttributeType, string> Attributes;
        public string Text;
        public int Count;

        public YamlNode(string ns, string localName, string text, IDictionary<AttributeType, string> attr = null)
        {
            Namespace = ns;
            LocalName = localName;
            Text = text;
            Attributes = attr;
            Count = 1;
        }

        public void Reference(string text, IDictionary<AttributeType, string> attr = null)
        {
            Text = text;
            Attributes = attr;

            ++Count;
        }

    }
}
