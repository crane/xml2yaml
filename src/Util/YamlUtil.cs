﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Xml2Yaml
{
    internal static class YamlUtil
    {
        private const char NullChar = (char)0;
        private const char Colon = ':';
        private const char Equal = '=';

        public const char NSSep = '\x1F';      // ASCII US (unit separator)
        public const char ContextSep = '\x0C'; // ASCII DC2 (device control 2)
        public const int DefaultReadBufSize = 16384;

        /// <summary>
        /// Encode String
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string EncodeXml(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return "";
            }
            else
            {
                return EncodeXmlString(Trim(text), true, true, true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="bLtGt"></param>
        /// <param name="bQuote"></param>
        /// <param name="bApos"></param>
        /// <returns></returns>
        private static string EncodeXmlString(string text, bool bLtGt, bool bQuote, bool bApos)
        {
            StringBuilder enCodeText = new StringBuilder(text);
            //encode
            enCodeText = enCodeText.Replace("&", "&amp;");
            if (bApos)
            {
                enCodeText = enCodeText.Replace("'", "&apos;");
            }
            if (bQuote)
            {
                enCodeText = enCodeText.Replace("\"", "&quot;");
            }
            if (bLtGt)
            {
                enCodeText = enCodeText.Replace("<", "&lt;").Replace(">", "&gt;");
            }

            return enCodeText.ToString();
        }


        public static bool IsWindowsPlatForm()
        {
            switch (System.Environment.OSVersion.Platform)
            {
                case PlatformID.Win32Windows:
                case PlatformID.Win32NT:
                case PlatformID.WinCE:
                case PlatformID.Win32S:
                    return true;
                default:
                    return false;
            }
        }

        public static string Escaped(char c)
        {
            const string c_escap = @"\";
            switch (c)
            {
                case '\0':
                case '\a':
                case '\b':
                case '\n':
                case '\r':
                case '\t':
                case '\v':
                    return c_escap + "x" + ((int)c).ToString("X");
                default:
                    return c.ToString();
            }
        }

        public static string Escaped(string s)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in s)
            {
                sb.Append(Escaped(c));
            }
            return sb.ToString();
        }

        public static bool IsTrue(string text)
        {
            return !string.IsNullOrEmpty(text)
               && (text == "1" || StringComparer.InvariantCultureIgnoreCase.Equals("true", text));
        }

        public static string Trim(string text)
        {
            return string.IsNullOrEmpty(text) ? text : text.Trim();
        }

       
        public static unsafe string PointerToString(char* str, int length)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < length; ++i)
            {
                sb.Append(*str);
                str++;
            }

            return sb.ToString();
        }

        public static unsafe string PointerToString(char* str)
        {
            StringBuilder sb = new StringBuilder();

            while (str != null && *str != NullChar)
            {
                sb.Append(*str);
                str++;
            }

            return sb.ToString();
        }

        public static unsafe string[] PointerToStringArray(char** strs)
        {
            List<string> list = new List<string>();

            while (*strs != null && **strs != NullChar)
            {
                list.Add(PointerToString(*strs));
                strs++;
            }

            return list.ToArray();
        }


        public static Stream OpenOrCreateOutput(string fileName)
        {
            if (File.Exists(fileName))
            {
                return File.Open(fileName, FileMode.OpenOrCreate | FileMode.Truncate, FileAccess.Write);
            }
            else
            {
                return File.Open(fileName, FileMode.CreateNew, FileAccess.Write);
            }
        }
    }
}
