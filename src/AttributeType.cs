﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xml2Yaml
{
    class AttributeType
    {
        public string NS
        {
            get;
            set;
        }
        public string LocalName
        {
            get;
            set;
        }

        public AttributeType()
        {
        }

        public AttributeType(KeyValuePair<string, string> pair)
        {
            NS = pair.Key;
            LocalName = pair.Value;
        }

    }
}
